

-- Insert  Gyms' Cities for the battles

INSERT into Gyms (City)
VALUES
    ('Lavender Town'),
    ('Cerulean City'),
    ('Pewter City'),
    ('Fuschia City'),
    ('Vermilion City')


-- Insert the few pokemons everyone will remember
INSERT into Pokemons (Name, URL)
VALUES
    ('Pikachu','https://img.pokemondb.net/sprites/x-y/normal/pikachu.png'),
    ('Bulbasaur','https://img.pokemondb.net/sprites/x-y/normal/bulbasaur.png'),
    ('Charmander','https://img.pokemondb.net/sprites/x-y/normal/charmander.png'),
    ('Squirtle','https://img.pokemondb.net/sprites/x-y/normal/squirtle.png')



-- insert more phase 1 pokemons

INSERT into Pokemons (Name,URL)
VALUES
    ('Arbok', 'https://img.pokemondb.net/sprites/x-y/normal/arbok.png'),
    ('Arcanine', 'https://img.pokemondb.net/sprites/x-y/normal/arcanine.png'),
    ('Articuno', 'https://img.pokemondb.net/sprites/x-y/normal/articuno.png'),
    ('Blastoise', 'https://img.pokemondb.net/sprites/x-y/normal/blastoise.png'),
    ('Butterfree', 'https://img.pokemondb.net/sprites/x-y/normal/butterfree.png'),
    ('Caterpie', 'https://img.pokemondb.net/sprites/x-y/normal/caterpie.png'),
    ('Charizard', 'https://img.pokemondb.net/sprites/x-y/normal/charizard.png'),
    ('Charmeleon', 'https://img.pokemondb.net/sprites/x-y/normal/charmeleon.png'),
    ('Fearow', 'https://img.pokemondb.net/sprites/x-y/normal/fearow.png'),
    ('Gengar', 'https://img.pokemondb.net/sprites/x-y/normal/gengar.png'),
    ('Golbat', 'https://img.pokemondb.net/sprites/x-y/normal/golbat.png'),
    ('Golem', 'https://img.pokemondb.net/sprites/x-y/normal/golem.png'),
    ('Gyarados', 'https://img.pokemondb.net/sprites/x-y/normal/gyarados.png'),
    ('Jigglypuff', 'https://img.pokemondb.net/sprites/x-y/normal/jigglypuff.png'),
    ('Kakuna', 'https://img.pokemondb.net/sprites/x-y/normal/kakuna.png'),
    ('Meowth', 'https://img.pokemondb.net/sprites/x-y/normal/meowth.png'),
    ('Metapod', 'https://img.pokemondb.net/sprites/x-y/normal/metapod.png'),
    ('Mewtwo', 'https://img.pokemondb.net/sprites/x-y/normal/mewtwo.png'),
    ('Mew', 'https://img.pokemondb.net/sprites/x-y/normal/mew.png'),
    ('Onix', 'https://img.pokemondb.net/sprites/x-y/normal/onix.png'),
    ('Pidgeotto', 'https://img.pokemondb.net/sprites/x-y/normal/pidgeotto.png'),
    ('Pidgey', 'https://img.pokemondb.net/sprites/x-y/normal/pidgey.png'),
    ('Ponyta', 'https://img.pokemondb.net/sprites/x-y/normal/ponyta.png'),
    ('Psyduck', 'https://img.pokemondb.net/sprites/x-y/normal/psyduck.png'),
    ('Raichu', 'https://img.pokemondb.net/sprites/x-y/normal/raichu.png'),
    ('Raticate', 'https://img.pokemondb.net/sprites/x-y/normal/raticate.png'),
    ('Rattata', 'https://img.pokemondb.net/sprites/x-y/normal/rattata.png'),
    ('Sandslash', 'https://img.pokemondb.net/sprites/x-y/normal/sandslash.png'),
    ('Snorlax', 'https://img.pokemondb.net/sprites/x-y/normal/snorlax.png'),
    ('Spearow', 'https://img.pokemondb.net/sprites/x-y/normal/spearow.png'),
    ('Starmie', 'https://img.pokemondb.net/sprites/x-y/normal/starmie.png'),
    ('Vaporeon', 'https://img.pokemondb.net/sprites/x-y/normal/vaporeon.png'),
    ('Venusaur', 'https://img.pokemondb.net/sprites/x-y/normal/venusaur.png'),
    ('Voltorb', 'https://img.pokemondb.net/sprites/x-y/normal/voltorb.png'),
    ('Wartortle', 'https://img.pokemondb.net/sprites/x-y/normal/wartortle.png'),
    ('Weedle', 'https://img.pokemondb.net/sprites/x-y/normal/weedle.png'),
    ('Weepinbell', 'https://img.pokemondb.net/sprites/x-y/normal/weepinbell.png'),
    ('Zubat', 'https://img.pokemondb.net/sprites/x-y/normal/zubat.png')


