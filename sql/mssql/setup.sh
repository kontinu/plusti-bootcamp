#!/bin/bash

/wait-for-it.sh -t 90 localhost:1433 -- echo "DB is up"

echo "[$(date)] DB is up" > /tmp/log.txt

sleep 20
for i in /*.sql; do
    echo "[$(date)] Executing $i" >> /tmp/log.txt
    /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P ${SA_PASSWORD:-"th3Passw0rd"} -i $i
done


echo "[$(date)] Migration done" >> /tmp/log.txt
