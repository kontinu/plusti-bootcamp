
CREATE TABLE Pokemons (
    ID  INT IDENTITY(1, 1) PRIMARY KEY NOT NULL   ,
    Name VARCHAR(100) NOT NULL,
    URL VARCHAR(256) NOT NULL
);

-- Create Gyms table where the battles take place
CREATE TABLE Gyms (
    City VARCHAR(100) NOT NULL
);
