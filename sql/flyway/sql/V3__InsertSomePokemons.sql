-- Insert the few pokemons everyone will remember
INSERT into Pokemons (Name, URL)
VALUES
    ('Pikachu','https://img.pokemondb.net/sprites/x-y/normal/pikachu.png'),
    ('Bulbasaur','https://img.pokemondb.net/sprites/x-y/normal/bulbasaur.png'),
    ('Charmander','https://img.pokemondb.net/sprites/x-y/normal/charmander.png')