# Kubernetes Intro

we can translate the current docker-compose to kubernetes.

- [compose service] NodeJs App => Deployment + ConfigMap + Service
- [compose service] MSSL => StatefulSet + ConfigMap + HeadlessService
- [compose network] myapp => Namespace


## Excercise

```
git clone https://gitlab.com/kontinu/plusti-bootcamp.git
cd plusti-bootcamp/

# create a directory for the manifests
mkdir -p k8s/manifests
```

## MSSQL in Kubernetes
the work has already been done, so go and take a look

```
ls k8s/mssql-in-gke/

cat k8s/mssql-in-gke/deployment.yaml

cat k8s/mssql-in-gke/service.yaml

cat k8s/mssql-in-gke/pvc.yaml
```

## Poke in K8s

we will start translating the docker-compose to kubernetes.

=== "Docker Compose"

    ``` yaml
    # DNS para service discovery, traducido a Service.metadata.name
    app:
      # traducido a spec.template.spec.containers
      image: ${APP_DOCKER_IMAGE:-registry.gitlab.com/kontinu/plusti-bootcamp/master/app}
      # no traduccion directa porque el Deployment es el controlador que ayuda con esto.
      restart: always
      # no existe traduccion directa, kubernetes por el momento solo nos sirve para correr apps.
      build:
        context: app
        dockerfile: Dockerfile
      # pasando variables de entorno traducido a spec.template.spec.containers.env
      environment:
        - DB_HOST=db
      # traducido a spec.template.spec.containers.env
      env_file:
        - app/.env.example
      # no necesita traduccion en K8s los pods forman parte de una misma subred, incluso cuando viven en diferente Namespace.
      networks:
        - myapp
      # traducido a spec.template.spec.containers.ports y para que sea accessible a Service.
      ports:
        - "8080:5000"


    ```

=== "Deployment"

    ``` yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: pokemons
      # estos labels no son funcionales!
      # por lo tanto no tienen que ser igual a los matchLabels ni labels de los pods
      labels:
        app: pokemons
    spec:
      replicas: 1
      selector:
        matchLabels:
          # tiene que hacer match con template.metadata.labels
          app: pokemons
      template:
        metadata:
          # tiene que hacer match con spec.selector.matchLabels
          labels:
            app: pokemons
        spec:
          containers:
            - name: mycontainer
              # asi configuramos el app usando variables de entorno.
              env:
                - name: DB_HOST
                  value: mssql
                - name: DB_USER
                  value: sa
                #! No usar texto plano, sino mejor secrets.
                - name: DB_PASSWORD
                  value: th3Passw0rd
              #! NUNCA USAR LATEST!
              image: registry.gitlab.com/kontinu/plusti-bootcamp/master/app
              ports:
              #* nuestra app escucha en el puerto 5000
                - containerPort: 5000
              resources: {}

    ```

=== "Service"

    ``` yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: pokemons
      labels:
        # no funcionales!
        service: for-pokemons
    spec:
      # balanceador interno
      type: NodePort # ClusterIP | NodePort | LoadBalancer
      ports:
        - port: 80 # puerto del servicio (que se expone a otras aplicaciones)
          targetPort: 5000 # puerto del contenedor
      selector:
        # match con los labels del pod
        app: pokemons

    ```


---
## Further Reading

- [kustomize](https://kubernetes.io/docs/tutorials/kustomize/)
- [Helm](https://helm.sh/)
- [Docs](https://kubernetes.io/docs/tutorials/kubectl/)
