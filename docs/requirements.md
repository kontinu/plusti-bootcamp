# Installation requirements

# Event 1

- [x] [git](https://git-scm.com/)
- [x] [Docker](https://www.docker.com/)
- [x] [Terraform](https://www.terraform.io/)
- [x] [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)
# Event 2

- [x] [Docker](https://www.docker.com/)
- [x] [Docker Compose](https://docs.docker.com/compose/install/)

# Event 3

- [x] [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [x] [Minikube](https://minikube.sigs.k8s.io/docs/start/)
- [x] [Helm](https://helm.sh/docs/intro/quickstart/)

K9s or Lens or both

- [Lens](https://lens.k8s.io/)
- [k9s](https://github.com/derailed/k9s)
