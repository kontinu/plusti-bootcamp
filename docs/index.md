# Welcome to Monitor Plus Cloud Bootcamp 2021

This is a website designed for you! in it youll find all the information you need to get started with the Monitor Plus Cloud Bootcamp



# Meet our app - Pokemons

![](static/img/pokedex-plusti.gif)

Very simple application that is composed of:

- NodeJS v14 application (Express + Handlebars)
- Microsoft SQL server 2017 as Database.
- Migration Schema (MSSQL)
