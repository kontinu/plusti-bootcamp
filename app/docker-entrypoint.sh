#!/bin/bash

/usr/src/app/wait-for-it.sh -t 90 ${DB_HOST}:${DB_PORT:-1433}

npm start
